# `selection` configures the selected modules via a list of triples.
# A triple contains the module name, its field and its credit points.
# Note that special modules need to be named exactly like in mpocalc.py,
#   i.e. Praktikum, Seminar I, Seminar II, Masterarbeit, Masterkolloquium, Schwerpunktkolloquium.
# The fields obviously need also be named exactly like in mpocalc.py,
#   i.e. Theorie, Software, Daten, Angewandt, Anwendungsfach, Sonstiges.

# This is a syntactically correct example. Switch out lines 10 to 22
# with your information, add modules, ...
selection = [
    ('Something from Theory', 'Theorie', 6),
    ('Something else from Data', 'Daten', 6),
    ('More Data', 'Daten', 4),
    ('Building Things', 'Angewandt', 6),
    ('Faster Things', 'Angewandt', 6),
    ('Something different', 'Anwendungsfach', 2),
    ('Theory again', 'Theorie', 4),
    ('Praktikum', 'Sonstiges', 7),
    ('Different all the way down', 'Anwendungsfach', 6),
    ('Schwerpunktkolloquium', 'Angewandt', 3),
    ('Seminar I', 'Daten', 4),
    ('Masterarbeit', 'Theorie', 27),
    ('Masterkolloquium', 'Theorie', 3)
]
