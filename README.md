# mpocalc

The script `mpocalc.py` checks a selection of modules if it conforms to
the regulations for "Master of Science, Informatik" at RWTE²H. Because
this course of studies consists mainly of electable courses, it can be
difficult to maintain an overview and keep track which modules you are
still allowed to take for extra credits. This script takes care of
doing the calculations on the basis of a listing of (to be) elected
courses.

Please note, that I cannot guarantee the full conformance with any
regulations that are currently in effect. I do not have the time to
update this software beyond my own studies. Also, no software can ever
cover all the details that are possible within the natural language
formulation of the rules. This script is merly a simple check that
a certain list of modules may add up to a M.Sc.

## Usage

This script requires Python 3 to run. Otherwise, no external libraries
needed. It shall be run in a terminal; no GUI at all.

Copy `modules.example.py` to `modules.py` and edit it to your needs.
It starts off with some commentary on the required information and
includes a runnable example. Sorry for not implementing a real config
file, but it should be a simple enough format.

Run `mpocalc.py`. It emits information, warnings and possibly errors
depending on your configuration (all to stdout, i.e. the terminal).
Only errors are circumstances which prevents your selection from being
a valid selection.

