#!/usr/bin/env python3

import sys
import logging
logging.basicConfig(level=logging.DEBUG, format='%(levelname)s: %(message)s')

# ((Name, min CP, max CP), ...)
fields = (('Theorie', 12, 35), ('Software', 0, 35), ('Daten', 0, 35), ('Angewandt', 0, 35), ('Anwendungsfach', 18, 18), ('Sonstiges', 0, 120))

# [(Name, min CP, max CP, required, adds to field CP), ...]
special_modules = [('Praktikum', 6, 8, True, False), ('Seminar I', 4, 4, True, True), ('Seminar II', 4, 4, False, True),
        ('Masterarbeit', 27, 27, True, False), ('Masterkolloquium', 3, 3, True, False), ('Schwerpunktkolloquium', 3, 3, True, False)]

max_cp = 120

from modules import selection

names = [ s[0] for s in selection ]
fl = [ s[0] for s in fields ]
spl = [ s[0] for s in special_modules ]

def check_field_completeness():
    fn = 0
    for f in ['Theorie', 'Software', 'Daten', 'Angewandt']:
        check = False
        for s in selection:
            if s[1] == f:
                check = True
                break
        if check:
            fn += 1
    if fn > 4:
        logging.error('Field Number is greater than 4. WTF?')
        return -1
    if fn < 3:
        logging.error('Field Number is less than 3.')
        return -1
    logging.info('Field Number check ran successfully.')
    return 0

def check_bounded_cp():
    bcp = 0
    for f in fields:
        fcp = 0
        for s in [s for s in selection if s[1] == f[0]]:
            if s[0] not in spl or special_modules[spl.index(s[0])][4]:
                fcp += s[2]
        bcp += min(fcp, f[2])
    for s in [ s for s in selection if s[0] in spl ]:
        if not special_modules[spl.index(s[0])][4]:
            bcp += s[2]
    for s in [ s for s in selection if s[1] not in fl and s[0] not in spl ]:
        bcp += s[2]
    logging.info('Bounded CP: ' + str(bcp))
    if bcp < max_cp:
        logging.error('Less bounded CP than required.')
        return -1
    elif bcp > max_cp:
        logging.warn('More bounded CP than required.')
        return 1
    else:
        logging.info('Bounded CP check ran successfully.')
        return 0

def check_field_cp():
    ret = 0
    for s in selection:
        if s[1] in fl:
            continue
        logging.warn('Module ' + s[0] + ' does not have a valid field.')
        ret = 1
    for f in fields:
        fcp = 0
        for s in [s for s in selection if s[1] == f[0]]:
            if s[0] not in spl or special_modules[spl.index(s[0])][4]:
                fcp += s[2]
            else:
                logging.info('Not adding module ' + s[0] + ' to field cp ' + f[0] + '.')
        logging.info('Field CP ' + f[0] + ': ' + str(fcp))
        if fcp < f[1]:
            logging.error('Field ' + f[0] + ' is too low on CP.')
            ret = -1
        if fcp > f[2]:
            logging.warn('Field ' + f[0] + ' is overbooked on CP.')
            ret = 1 if ret == 0 else ret
        elif fcp < f[2]:
            logging.info('There is still free space in field ' + f[0] + '.')
    if ret == 0:
        logging.info('Field CP check ran successfully.')
    return ret

def check_special_modules():
    ret = 0
    for sp in special_modules:
        if sp[0] not in names:
            logging.info('Special Module ' + sp[0] + ' not in selection.')
            if sp[3]:
                logging.error('Required module ' + sp[0] + ' not in selection.')
                ret = -1
            continue
        if sp[1] > selection[names.index(sp[0])][2]:
            logging.error('Special module ' + sp[0] + ' is low on CPs.')
        if sp[2] < selection[names.index(sp[0])][2]:
            logging.warn('Special module ' + sp[0] + ' is overbooked on CPs.')
            ret = 1 if ret == 0 else ret
        if sp[4] and selection[names.index(sp[0])][1] not in fl:
            logging.error('Module ' + sp[0] + ' needs a valid field.')
            ret = -1
    if ret == 0:
        logging.info('Special Module check ran successfully.')
    return ret


def check_max_cp():
    cp = 0
    for s in selection:
        cp += s[2]
    logging.info('Cumulative CP: ' + str(cp))
    if cp < max_cp:
        logging.error('Less cumulative CP than required.')
        return -1
    elif cp > max_cp:
        logging.warn('More cumulative CP than required.')
        return 1
    else:
        logging.info('Cumulative CP check ran successfully.')
        return 0

checks = [check_max_cp, check_special_modules, check_field_cp, check_bounded_cp, check_field_completeness]
rets = []
for c in checks:
    rets.append(c())

if 0 in set(rets) and len(set(rets)) == 1:
    logging.info('All good.')
elif -1 in rets:
    logging.error('There have been errors encountered.')
    sys.exit(-1)
else:
    logging.warn('There have been unsuccessful checks.')

